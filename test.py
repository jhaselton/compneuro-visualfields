from scipy import ndimage, signal, misc
import matplotlib.pyplot as plt
import numpy as np
from math import pi, sqrt
from random import randint
from sklearn import decomposition
class picture:
    count = 0
    picturelist = []
    def __init__(self, filename):
        self.imageArray = dRemove(ndimage.imread(filename))
        self.name = filename
        picture.picturelist.append(self)
        picture.count += 1
        self.number=picture.count
        self.rgcImage = None
        self.scImage = None
        self.randomArrays = None
    def display(self):
        plt.figure(self.number)
        plt.imshow(self.imageArray, cmap = plt.cm.Greys_r)
        plt.axis('off')
    def toString(self):
        print(self.name)
    @staticmethod
    def listPictures():
        for i in picture.picturelist:
            i.toString()
    @staticmethod
    def displayPictures():
        for i in range(len(picture.picturelist)):
            picture.picturelist[i].display()
    def scanRgc(self, field):
        self.rgcImage= signal.convolve2d(self.imageArray,field, mode='same')
    def scanSc(self, field):
        self.scImage= signal.convolve2d(self.imageArray,field, mode='same')
    def randomSampling(self,x=50000,size=16):
        output = []
        temp = []
        temp2 = []
        for i in range(x):
            x = randint(0,self.imageArray.shape[0]-(size+1))
            y = randint(0,self.imageArray.shape[1]-(size+1))
            for i in range(size):
                for j in range(size):
                    temp.append(self.imageArray[x+i][y+j])
                temp2.append(temp)
                temp = []
            output.append(temp2)
            temp2 = []
        self.randomArrays = sampleArray(output)
    @staticmethod
    def randomSampleAll(x=50000,size=16):
        output = []
        temp = []
        temp2 = []
        for i in range(x):
            image = randint(0,len(picture.picturelist)-1)
            x = randint(0,picture.picturelist[image].imageArray.shape[0]-(size+1))
            y = randint(0,picture.picturelist[image].imageArray.shape[1]-(size+1))
            for i in range(size):
                for j in range(size):
                    temp.append(picture.picturelist[image].imageArray[x+i][y+j])
                temp2.append(temp)
                temp = []
            output.append(temp2)
            temp2 = []
        return sampleArray(output)
    def saveRgc(self, outname):
        misc.imsave(outname, self.rgcImage)
    def saveSc(self, outname):
        misc.imsave(outname, self.scImage)
    
def RGC(size=16, s=1.6, k=5):
    x = np.arange((-size+1)/2, (size)/2, 1, float)
    y = x[:,np.newaxis]
    temp = (1.0/(2*pi*(s**2)))*np.exp(-(1.0/(2*(s**2)))*(x**2 + y**2))
    temp2 = (1.0/(2*pi*(k**2)*(s**2)))*np.exp(-(1.0/(2*(k**2)*(s**2)))*(x**2 + y**2))
    temp3 = (temp - temp2)
    return (normalize(temp3))

def SC(size=16,sx=4,sy=7,fx=.1,fy=0):
    x = np.arange((-size+1)/2, (size)/2, 1, float)
    y = x[:,np.newaxis]
    return (((1.0/2*pi*sx*sy)*np.exp(-.5*(((x**2)/(sx**2))+((y**2)/(sy**2)))))*np.cos((2*pi*(fx*x+fy*y))))
    
def pcaComponents(array, components=10):
    pcatemp = decomposition.PCA(n_components=components)
    return pcatemp.fit(array).components_

def icaComponents(array, components=10):
    icatemp = decomposition.FastICA(n_components=components)
    return icatemp.fit(array).components_
    
def dRemove(image):
    temparray = image[:,:,0]
    return temparray

def normalize(array):
    normalizedArray = array
    mean = np.mean(normalizedArray)
    std = np.std(array)
    normalizedArray[:] = [(z-mean)/std for z in normalizedArray]
    return normalizedArray

def linearize(array):
    line = []
    for i in array:
        for j in i:
            line.append(j)
    return line
def delinearize(array):
    size = sqrt(len(array)) #Would generally need a better test to make sure size is an integer in real code, shouldn't matter here.
    outarray = []
    temp = []
    counter = 1
    for i in range(len(array)):
        if size!=counter:
            temp.append(array[i])
            counter+=1
        else:
            outarray.append(temp)
            temp=[]
            counter=1
    return outarray
def sampleArray(samples):
    outArray = []
    for i in samples:
        outArray.append(linearize(i))
    return outArray

def main():
    picture1 = picture('aspen.jpg')
    picture2 = picture('tree.jpg')
    picture3 = picture('mountain.jpg')
    cell = RGC()
    scell = SC()
    #for i in range(len(picture.picturelist)):
        #picture.picturelist[i].scanRgc(cell)
        #picture.picturelist[i].scanSc(scell)
        #picture.picturelist[i].saveRgc(picture.picturelist[i].name+"_Rgc.png")
        #picture.picturelist[i].saveSc(picture.picturelist[i].name+"_Sc.png")
    #misc.imsave("mountain_Array.png",picture3.imageArray)
    #test = picture.randomSampleAll(x=50000,size=16)
    #pcacomp = pcaComponents(test)
    #icacomp = icaComponents(test)
    #for i in range(10):
        #plt.figure(i)
        #plt.imshow(delinearize(icacomp[i]))
        #plt.axis("off")
        #plt.savefig("ICA_Comp"+str(i)+".jpg")
    #test.scanRgc(cell)
    #plt.imshow(cell)
    #plt.colorbar()
    #print(cell)
    #test4.scanSc(scell)
    #test.randomSampling()
    #test2 = pcaComponents(test.randomArrays)
    #plt.imshow(delinearize(test2[1]))
    #testsample = randomSampling(test.imageArray)
    #print(len(testsample[0]))
    #misc.imsave('RGCaspen.png',test4.rgcImage)
    #plt.imshow(test4.rgcImage, cmap = plt.cm.Greys_r)
    #pcatest = decomposition.PCA(n_components=10)
    #pcatest.fit(testsample)
    #test2 = pcatest.components_
    #testarray = delinearize(test2[0])
    #plt.imshow(testarray)
    
if __name__ == "__main__":
    main()